use std::mem;
use std::ptr;
use std::slice;

extern crate common;
use common::protocol::*;


#[cfg(asmjs)]
pub mod buffer {
    use std::mem;

    unsafe fn buffer_into_parts(b: Box<[u8]>) -> (*mut u8, usize) {
        let mut b = b;
        let ptr = b.as_mut_ptr();
        let len = b.len();
        mem::forget(b);
        (ptr, len)
    }

    unsafe fn buffer_from_parts(ptr: *mut u8, len: usize) -> Box<[u8]> {
        Vec::from_raw_parts(ptr, len, len).into_boxed_slice()
    }

    /// Allocate a new buffer of size `len`, and return its pointer.
    #[no_mangle]
    pub unsafe extern "C" fn buffer_new(len: usize) -> *mut u8 {
        let b = vec![0; len].into_boxed_slice();
        let (ptr, _len) = buffer_into_parts(b);
        ptr
    }

    /// Deallocate a buffer.
    #[no_mangle]
    pub unsafe extern "C" fn buffer_delete(ptr: *mut u8,
                                           len: usize) {
        let b = buffer_from_parts(ptr, len);
        mem::drop(b)
    }
}


pub struct OccProtocol {
    p: Protocol,
    cur_event: Option<OutputEvent>,
}

/// Allocate and initialize a new protocol state object.
#[no_mangle]
pub unsafe extern "C" fn occ_protocol_new(initiator: u8) -> *mut OccProtocol {
    let pw = Box::new(OccProtocol {
        p: Protocol::new(initiator != 0),
        cur_event: None,
    });
    Box::into_raw(pw)
}

/// Deallocate a protocol state object.
#[no_mangle]
pub unsafe extern "C" fn occ_protocol_delete(pw: *mut OccProtocol) {
    let pw = Box::from_raw(pw);
    mem::drop(pw);
}


pub const EVENT_NONE: u8 = 0;
pub const EVENT_RECV: u8 = 1;
pub const EVENT_OUTGOING: u8 = 2;
pub const EVENT_HANDSHAKE_FINISHED: u8 = 3;
pub const EVENT_ERROR: u8 = 4;
pub const EVENT_FATAL_ERROR: u8 = 5;

fn event_type(evt: &Option<OutputEvent>) -> u8 {
    match *evt {
        None => EVENT_NONE,
        Some(OutputEvent::Recv(_)) => EVENT_RECV,
        Some(OutputEvent::Outgoing(_)) => EVENT_OUTGOING,
        Some(OutputEvent::HandshakeFinished(_)) => EVENT_HANDSHAKE_FINISHED,
        Some(OutputEvent::Error(_)) => EVENT_ERROR,
        Some(OutputEvent::FatalError(_)) => EVENT_FATAL_ERROR,
    }
}

/// Pop an event from the protocol's output event queue, put it into the "current event" buffer,
/// and return its type.  After this, all the output-event accessor functions will return data
/// concerning the popped event.
#[no_mangle]
pub unsafe extern "C" fn occ_protocol_next_event(pw: *mut OccProtocol) -> u8 {
    let pw = &mut *pw;
    pw.cur_event = pw.p.output();
    event_type(&pw.cur_event)
}

/// Returns a pointer to the current `RECV` event's data.  The length of the data is stored in
/// `*len_out`.
#[no_mangle]
pub unsafe extern "C" fn occ_protocol_recv_data(pw: *mut OccProtocol,
                                                len_out: *mut usize) -> *const u8 {
    match (*pw).cur_event {
        Some(OutputEvent::Recv(ref msg)) => {
            ptr::write(len_out, msg.len());
            msg.as_ptr()
        },
        _ => panic!("not a recv event"),
    }
}

/// Returns a pointer to the current `OUTGOING` event's data.  The length of the data is stored in
/// `*len_out`.
#[no_mangle]
pub unsafe extern "C" fn occ_protocol_outgoing_data(pw: *mut OccProtocol,
                                                    len_out: *mut usize) -> *const u8 {
    match (*pw).cur_event {
        Some(OutputEvent::Outgoing(ref msg)) => {
            ptr::write(len_out, msg.len());
            msg.as_ptr()
        },
        _ => panic!("not an outgoing event"),
    }
}

pub const CHANNEL_BINDING_TOKEN_LEN: usize = 32;

/// Returns a pointer to the current `HANDSHAKE_FINISHED` event's channel binding token.  The length of the token is `CHANNEL_BINDING_TOKEN_LEN`.
#[no_mangle]
pub unsafe extern "C" fn occ_protocol_handshake_finished_channel_binding_token(
        pw: *mut OccProtocol) -> *const u8 {
    match (*pw).cur_event {
        Some(OutputEvent::HandshakeFinished(ref cb_token)) => {
            // Compile-time assertion that the `cb_token` really has the right length.
            let cb_token: &[u8; CHANNEL_BINDING_TOKEN_LEN] = cb_token.bytes();
            cb_token.as_ptr()
        },
        _ => panic!("not a handshake_finished event"),
    }
}

/// Returns a pointer to the current `ERROR` event's message.  The length of the data is stored in
/// `*len_out`.
#[no_mangle]
pub unsafe extern "C" fn occ_protocol_error_message(pw: *mut OccProtocol,
                                                    len_out: *mut usize) -> *const u8 {
    match (*pw).cur_event {
        Some(OutputEvent::Error(ref msg)) |
        Some(OutputEvent::FatalError(ref msg)) => {
            ptr::write(len_out, msg.len());
            msg.as_ptr()
        },
        _ => panic!("not an error event"),
    }
}


/// Dispatch an `OPEN` event to the protocol.
#[no_mangle]
pub unsafe extern "C" fn occ_protocol_open(pw: *mut OccProtocol) {
    (*pw).p.input(InputEvent::Open);
}

/// Dispatch a `SEND` event to the protocol.
#[no_mangle]
pub unsafe extern "C" fn occ_protocol_send(pw: *mut OccProtocol,
                                           msg: *const u8,
                                           len: usize) {
    (*pw).p.input(InputEvent::Send(slice::from_raw_parts(msg, len)));
}

/// Dispatch an `INCOMING` event to the protocol.
#[no_mangle]
pub unsafe extern "C" fn occ_protocol_incoming(pw: *mut OccProtocol,
                                               msg: *const u8,
                                               len: usize) {
    (*pw).p.input(InputEvent::Incoming(slice::from_raw_parts(msg, len)));
}
