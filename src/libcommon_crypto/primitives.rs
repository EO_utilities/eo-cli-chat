use std::mem;
use std::ptr;

use error::{Error, Result};

macro_rules! check {
    ($err:ident, $e:expr) => {
        {
            let ret = $e;
            if ret < 0 {
                return Err(Error::$err);
            }
            ret
        }
    };
}


use libc::{c_int, c_longlong};
//#[cfg(not(target_arch = "wasm32"))]
//use libc::{c_int, c_longlong};

// liblibc provides no typedefs on wasm32, for some reason.
#[cfg(target_arch = "wasm32")]
#[allow(bad_style)]
type c_int = i32;
#[cfg(target_arch = "wasm32")]
#[allow(bad_style)]
type c_longlong = i64;

#[link(name = "sodium")]
extern "C" {
    // `len` should have type `c_longlong`, but we can't pass i64's to javascript.
    fn randombytes(buf: *mut u8, len: i32);

    fn crypto_scalarmult_curve25519(q: *mut u8, n: *const u8, p: *const u8) -> c_int;
    fn crypto_scalarmult_curve25519_base(q: *mut u8, n: *const u8) -> c_int;

    fn crypto_aead_chacha20poly1305_ietf_encrypt(c: *mut u8,
                                                 clen_p: *mut c_longlong,
                                                 m: *const u8,
                                                 mlen: c_longlong,
                                                 ad: *const u8,
                                                 adlen: c_longlong,
                                                 nsec: *const u8,
                                                 npub: *const u8,
                                                 k: *const u8) -> c_int;
    fn crypto_aead_chacha20poly1305_ietf_decrypt(m: *mut u8,
                                                 mlen_p: *mut c_longlong,
                                                 nsec: *mut u8,
                                                 c: *const u8,
                                                 clen: c_longlong,
                                                 ad: *const u8,
                                                 adlen: c_longlong,
                                                 npub: *const u8,
                                                 k: *const u8) -> c_int;

    fn crypto_hash_sha256(out: *mut u8,
                          m: *const u8,
                          mlen: c_longlong) -> c_int;
}

macro_rules! byte_array {
    ($name:ident [ $len:expr ]) => {
        #[derive(Clone, PartialEq, Eq, Debug)]
        pub struct $name([u8; $len]);

        impl $name {
            fn new() -> $name {
                $name([0; $len])
            }

            pub fn bytes(&self) -> &[u8; $len] {
                &self.0
            }
        }
    };
}


byte_array!(DHPrivateKey[32]);
byte_array!(DHPublicKey[32]);
byte_array!(DHOutput[32]);
pub const DH_LEN: usize = 32;

pub fn dh_generate_keypair() -> Result<(DHPublicKey, DHPrivateKey)> {
    unsafe {
        let mut priv_key = DHPrivateKey::new();
        let mut pub_key = DHPublicKey::new();

        randombytes(priv_key.0.as_mut_ptr(), priv_key.0.len() as i32);

        check!(ScalarMultBase,
               crypto_scalarmult_curve25519_base(pub_key.0.as_mut_ptr(), priv_key.0.as_ptr()));

        Ok((pub_key, priv_key))
    }
}

pub fn dh(_my_public: &DHPublicKey,
          my_private: &DHPrivateKey,
          other_public: &DHPublicKey) -> Result<DHOutput> {
    unsafe {
        let mut output = DHOutput::new();
        check!(ScalarMult, 
               crypto_scalarmult_curve25519(output.0.as_mut_ptr(),
                                            my_private.0.as_ptr(),
                                            other_public.0.as_ptr()));
        Ok(output)
    }
}


byte_array!(CipherKey[32]);
pub type CipherNonce = u64;
pub const MAX_NONCE: CipherNonce = 0xffff_ffff_ffff_ffff;

// crypto_aead_chacha20poly1305_ABYTES
pub const CIPHER_ABYTES: usize = 16;

fn nonce_bytes(n: &CipherNonce) -> &[u8; 8] {
    assert!(mem::size_of::<CipherNonce>() == 8);
    unsafe { mem::transmute(n) }
}

pub fn cipher_encrypt(k: &CipherKey,
                      n: CipherNonce,
                      ad: &[u8],
                      plaintext: &[u8]) -> Result<Box<[u8]>> {
    unsafe {
        // "At most mlen + crypto_aead_chacha20poly1305_ABYTES bytes are put into c, and the actual
        // number of bytes is stored into clen unless clen is a NULL pointer."
        let mut output = vec![0; plaintext.len() + CIPHER_ABYTES];
        let mut output_len = 0;

        let mut nonce = [0; 12];
        nonce[..8].copy_from_slice(nonce_bytes(&n));

        let plaintext_len = plaintext.len() as c_longlong;
        assert!(plaintext_len as usize == plaintext.len());
        let ad_len = ad.len() as c_longlong;
        assert!(ad_len as usize == ad.len());

        check!(AeadEncrypt,
               crypto_aead_chacha20poly1305_ietf_encrypt(output.as_mut_ptr(),
                                                         &mut output_len,
                                                         plaintext.as_ptr(),
                                                         plaintext_len,
                                                         ad.as_ptr(),
                                                         ad_len,
                                                         ptr::null(),
                                                         nonce.as_ptr(),
                                                         k.0.as_ptr()));

        output.set_len(output_len as usize);
        Ok(output.into_boxed_slice())
    }
}

pub fn cipher_decrypt(k: &CipherKey,
                      n: CipherNonce,
                      ad: &[u8],
                      ciphertext: &[u8]) -> Result<Box<[u8]>> {
    unsafe {
        // "At most clen - crypto_aead_chacha20poly1305_ABYTES bytes will be put into m."
        assert!(ciphertext.len() >= CIPHER_ABYTES);
        let mut output = vec![0; ciphertext.len() - CIPHER_ABYTES];
        let mut output_len = 0;

        let mut nonce = [0; 12];
        nonce[..8].copy_from_slice(nonce_bytes(&n));

        let ciphertext_len = ciphertext.len() as c_longlong;
        assert!(ciphertext_len as usize == ciphertext.len());
        let ad_len = ad.len() as c_longlong;
        assert!(ad_len as usize == ad.len());

        check!(AeadDecrypt,
               crypto_aead_chacha20poly1305_ietf_decrypt(output.as_mut_ptr(),
                                                         &mut output_len,
                                                         ptr::null_mut(),
                                                         ciphertext.as_ptr(),
                                                         ciphertext_len,
                                                         ad.as_ptr(),
                                                         ad_len,
                                                         nonce.as_ptr(),
                                                         k.0.as_ptr()));

        output.set_len(output_len as usize);
        Ok(output.into_boxed_slice())
    }
}

pub fn cipher_rekey(k: &CipherKey) -> Result<CipherKey> {
    let encrypted = cipher_encrypt(k, MAX_NONCE, &[], &[0; 32])?;
    let mut output = CipherKey::new();
    output.0.copy_from_slice(&encrypted[..32]);
    Ok(output)
}


byte_array!(HashOutput[32]);
pub const HASH_LEN: usize = 32;
pub const HASH_BLOCK_LEN: usize = 64;

pub fn hash(data: &[u8]) -> HashOutput {
    unsafe {
        let mut output = HashOutput::new();

        let result = crypto_hash_sha256(output.0.as_mut_ptr(),
                                        data.as_ptr(),
                                        data.len() as c_longlong);
        // The current implementation never fails
        assert!(result == 0, "sha256 failed");

        output
    }
}


impl DHPublicKey {
    pub fn from_bytes(x: &[u8]) -> DHPublicKey {
        let mut h = DHPublicKey::new();
        assert!(x.len() == h.0.len());
        h.0.copy_from_slice(x);
        h
    }
}

impl CipherKey {
    pub fn from_hash_truncate(x: &HashOutput) -> CipherKey {
        // If HASHLEN was 64 instead of 32, we'd need to truncate temp_k to 32 bytes here.
        CipherKey(x.0.clone())
    }
}

impl HashOutput {
    pub fn from_bytes_unhashed(x: &[u8]) -> HashOutput {
        let mut h = HashOutput::new();
        if x.len() <= h.0.len() {
            h.0[..x.len()].copy_from_slice(x)
        } else {
            let len = h.0.len();
            h.0.copy_from_slice(&x[..len])
        }
        h
    }
}
